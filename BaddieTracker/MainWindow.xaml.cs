﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BaddieTracker
{
    public partial class MainWindow : Window
    {
        private Model m_model;
        private Thread m_imageThread;
        private string m_clipboard = "";
        private bool m_selectAll = false;

        public MainWindow()
        {
            InitializeComponent();

            m_model = new Model();
            DataContext = m_model;
        }


        public void RunImageSet(string setname)
        {
            m_model.IsSearchButtonEnabled = false;
            m_model.ProgressLabel = "Searching...";
            m_imageThread = new Thread(() =>
            {
                int size = Config.ImageSets[setname];
                for (int i = 1; i <= size; ++i)
                {
                    Dispatcher.Invoke(() => {
                        m_model.SetActiveImage($"{setname}{i}");
                        m_model.ProgressAmount = (double)i / (double)size;
                    });
                    Thread.Sleep(500);
                }
                Dispatcher.Invoke(() => m_model.ResetSearch());
            });
            m_imageThread.Start();
        }

        private static void ErrorBox(string msg, string title = "Tracking Error")
        {
            MessageBox.Show(
                msg,
                title,
                MessageBoxButton.OK,
                MessageBoxImage.Error,
                MessageBoxResult.OK);
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            // TODO: checks and message boxes

            if (m_model.SearchText.ToLower() == "jimmy")
            {
                if (m_model.AdminTrackingUnrestricted)
                {
                    RunImageSet("jimmy");
                    return;
                }
                else
                {
                    MessageBox.Show("Don't be silly, Jimmy's your teammate! Why would you want to search for him?", "Search Cancelled", MessageBoxButton.OK, MessageBoxImage.Question);
                    return;
                }
            }

            if (m_model.SearchText.ToLower() != Config.SearchName.ToLower())
            {
                RunImageSet("wrong");
                return;
            }

            if (!m_model.TrackingZoom)
            {
                RunImageSet("nozoom");
                return;
            }

            if (m_model.TrackingEnhance)
            {
                RunImageSet("good");
                return;
            }

            if (m_model.TrackingEntrance)
            {
                RunImageSet("entrance");
                return;
            }
            if (m_model.TrackingJustDance)
            {
                RunImageSet("justdance");
                return;
            }
            if (m_model.TrackingHeroStance)
            {
                RunImageSet("herostance");
                return;
            }
        }

        private void MiKiller_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(
                "No! You can't even manage to keep up with one killer, let alone two!",
                "Admin Access Required",
                MessageBoxButton.OK,
                MessageBoxImage.Exclamation);
        }

        private void MiDelete_Click(object sender, RoutedEventArgs e)
        {
            if (m_selectAll)
            {
                gMain.Visibility = Visibility.Hidden;
            }
            else
            {
                ((MenuItem)sender).Visibility = Visibility.Collapsed;
            }
        }

        private void MiSelectAll_Click(object sender, RoutedEventArgs e)
        {
            m_selectAll = true;
        }

        private void MiPaste_Click(object sender, RoutedEventArgs e)
        {
            if (m_clipboard == "Cut")
            {
                MenuItem newCut = new MenuItem { Header = "Cut" };
                newCut.Click += MiCut_Click;
                miEdit.Items.Insert(3, newCut);
            }
            else if (m_clipboard == "Copy")
            {
                MenuItem newCopy = new MenuItem { Header = "Copy" };
                newCopy.Click += MiCopy_Click;
                miEdit.Items.Insert(3, newCopy);
            }
        }

        private void MiCopy_Click(object sender, RoutedEventArgs e)
        {
            m_clipboard = "Copy";
        }

        private void MiCut_Click(object sender, RoutedEventArgs e)
        {
            ((MenuItem)sender).Visibility = Visibility.Collapsed;
            m_clipboard = "Cut";
        }

        private void MiNew_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("File support is not done, sorry :(");
        }

        private void MiOpen_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("File support is not done, sorry :(");
        }

        private void MiSave_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("File support is not done, sorry :(");
        }

        private void MiExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MiAbout_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow aw = new AboutWindow();
            aw.ShowDialog();
        }

        private void MiAdminDoubleTheKiller_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Still no! Why would you even click this? You are literally the worst detective ever!", "One is enough!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
    }
}

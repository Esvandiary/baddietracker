﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;

namespace BaddieTracker
{
    public class Model : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        private bool m_locatorGPS = false;
        public bool LocatorGPS
        {
            get { return m_locatorGPS; }
            set { m_locatorGPS = value; OnPropertyChanged(nameof(LocatorGPS)); }
        }

        private bool m_locatorAGPS = false;
        public bool LocatorAGPS
        {
            get { return m_locatorAGPS; }
            set { m_locatorAGPS = value; OnPropertyChanged(nameof(LocatorAGPS)); }
        }

        private bool m_locatorDGPS = false;
        public bool LocatorDGPS
        {
            get { return m_locatorDGPS; }
            set { m_locatorDGPS = value; OnPropertyChanged(nameof(LocatorDGPS)); }
        }

        private bool m_locatorGPRS = false;
        public bool LocatorGPRS
        {
            get { return m_locatorGPRS; }
            set { m_locatorGPRS = value; OnPropertyChanged(nameof(LocatorGPRS)); }
        }

        private bool m_locatorOSHA = false;
        public bool LocatorOSHA
        {
            get { return m_locatorOSHA; }
            set { m_locatorOSHA = value; OnPropertyChanged(nameof(LocatorOSHA)); }
        }

        private bool m_locatorSATA = false;
        public bool LocatorSATA
        {
            get { return m_locatorSATA; }
            set { m_locatorSATA = value; OnPropertyChanged(nameof(LocatorSATA)); }
        }

        private bool m_locatorSETI = false;
        public bool LocatorSETI
        {
            get { return m_locatorSETI; }
            set { m_locatorSETI = value; OnPropertyChanged(nameof(LocatorSETI)); }
        }

        private bool m_locatorDMA = false;
        public bool LocatorDMA
        {
            get { return m_locatorDMA; }
            set { m_locatorDMA = value; OnPropertyChanged(nameof(LocatorDMA)); }
        }

        private bool m_locatorCDMA = false;
        public bool LocatorCDMA
        {
            get { return m_locatorCDMA; }
            set { m_locatorCDMA = value; OnPropertyChanged(nameof(LocatorCDMA)); }
        }

        private bool m_locatorDMCA = false;
        public bool LocatorDMCA
        {
            get { return m_locatorDMCA; }
            set { m_locatorDMCA = value; OnPropertyChanged(nameof(LocatorDMCA)); }
        }

        private bool m_locatorMDMA = false;
        public bool LocatorMDMA
        {
            get { return m_locatorMDMA; }
            set { m_locatorMDMA = value; OnPropertyChanged(nameof(LocatorMDMA)); }
        }

        // ------------------------

        private bool m_ShowdownMinions = false;
        public bool ShowdownMinions
        {
            get { return m_ShowdownMinions; }
            set { m_ShowdownMinions = value; OnPropertyChanged(nameof(ShowdownMinions)); }
        }

        private bool m_ShowdownExplosions = false;
        public bool ShowdownExplosions
        {
            get { return m_ShowdownExplosions; }
            set { m_ShowdownExplosions = value; OnPropertyChanged(nameof(ShowdownExplosions)); }
        }

        private int m_ShowdownMainChars = 1;
        public int ShowdownMainChars
        {
            get { return m_ShowdownMainChars; }
            set { m_ShowdownMainChars = value; OnPropertyChanged(nameof(ShowdownMainChars)); }
        }

        private int m_ShowdownPunQuality = 0;
        public int ShowdownPunQuality
        {
            get { return m_ShowdownPunQuality; }
            set { m_ShowdownPunQuality = 0; OnPropertyChanged(nameof(ShowdownPunQuality)); }
        }

        // ------------------------

        private bool m_TrackingZoom = false;
        public bool TrackingZoom
        {
            get { return m_TrackingZoom; }
            set { m_TrackingZoom = value; OnPropertyChanged(nameof(TrackingZoom)); }
        }

        private void SetTrackingMode(bool enhance, bool entrance, bool justdance, bool herostance)
        {
            m_TrackingEnhance = enhance;
            m_TrackingEntrance = entrance;
            m_TrackingJustDance = justdance;
            m_TrackingHeroStance = herostance;
            OnPropertyChanged(nameof(TrackingEnhance));
            OnPropertyChanged(nameof(TrackingEntrance));
            OnPropertyChanged(nameof(TrackingJustDance));
            OnPropertyChanged(nameof(TrackingHeroStance));
        }

        private bool m_TrackingEnhance = true;
        public bool TrackingEnhance
        {
            get { return m_TrackingEnhance; }
            set { SetTrackingMode(value, false, false, false); }
        }

        private bool m_TrackingEntrance = false;
        public bool TrackingEntrance
        {
            get { return m_TrackingEntrance; }
            set { SetTrackingMode(false, value, false, false); }
        }

        private bool m_TrackingJustDance = false;
        public bool TrackingJustDance
        {
            get { return m_TrackingJustDance; }
            set { SetTrackingMode(false, false, value, false); }
        }

        private bool m_TrackingHeroStance = false;
        public bool TrackingHeroStance
        {
            get { return m_TrackingHeroStance; }
            set { SetTrackingMode(false, false, false, value); }
        }

        private int m_TrackingSearchGridRes = 50;
        public int TrackingSearchGridRes
        {
            get { return m_TrackingSearchGridRes; }
            set { m_TrackingSearchGridRes = value; OnPropertyChanged(nameof(TrackingSearchGridRes)); }
        }

        private int m_TrackingClueUsefulness = 0;
        public int TrackingClueUsefulness
        {
            get { return m_TrackingClueUsefulness; }
            set { m_TrackingClueUsefulness = 0; OnPropertyChanged(nameof(TrackingClueUsefulness)); }
        }

        // ------------------------

        private bool m_LabTestSafetyGoggles = false;
        public bool LabTestSafetyGoggles
        {
            get { return m_LabTestSafetyGoggles; }
            set { m_LabTestSafetyGoggles = value; OnPropertyChanged(nameof(LabTestSafetyGoggles)); }
        }

        private bool m_LabTestSafetyDance = false;
        public bool LabTestSafetyDance
        {
            get { return m_LabTestSafetyDance; }
            set { m_LabTestSafetyDance = value; OnPropertyChanged(nameof(LabTestSafetyDance)); }
        }

        private int m_LabTestMontageDuration = 20;
        public int LabTestMontageDuration
        {
            get { return m_LabTestMontageDuration; }
            set { m_LabTestMontageDuration = value; OnPropertyChanged(nameof(LabTestMontageDuration)); }
        }

        private int m_LabTestMontageWorkDone = 10;
        public int LabTestMontageWorkDone
        {
            get { return m_LabTestMontageWorkDone; }
            set { m_LabTestMontageWorkDone = value; OnPropertyChanged(nameof(LabTestMontageWorkDone)); }
        }

        private int m_LabTestAccuracy = 0;
        public int LabTestAccuracy
        {
            get { return m_LabTestAccuracy; }
            set { m_LabTestAccuracy = 0; OnPropertyChanged(nameof(LabTestAccuracy)); }
        }

        // ------------------------

        private bool m_DatabaseNCIC = false;
        public bool DatabaseNCIC
        {
            get { return m_DatabaseNCIC; }
            set { m_DatabaseNCIC = value; OnPropertyChanged(nameof(DatabaseNCIC)); }
        }

        private bool m_DatabaseAFIS = false;
        public bool DatabaseAFIS
        {
            get { return m_DatabaseAFIS; }
            set { m_DatabaseAFIS = value; OnPropertyChanged(nameof(DatabaseAFIS)); }
        }

        private bool m_DatabaseCODIS = false;
        public bool DatabaseCODIS
        {
            get { return m_DatabaseCODIS; }
            set { m_DatabaseCODIS = value; OnPropertyChanged(nameof(DatabaseCODIS)); }
        }

        private bool m_DatabaseCCH = false;
        public bool DatabaseCCH
        {
            get { return m_DatabaseCCH; }
            set { m_DatabaseCCH = value; OnPropertyChanged(nameof(DatabaseCCH)); }
        }

        private bool m_DatabaseIRC = false;
        public bool DatabaseIRC
        {
            get { return m_DatabaseIRC; }
            set { m_DatabaseIRC = value; OnPropertyChanged(nameof(DatabaseIRC)); }
        }

        private bool m_DatabaseNICS = false;
        public bool DatabaseNICS
        {
            get { return m_DatabaseNICS; }
            set { m_DatabaseNICS = value; OnPropertyChanged(nameof(DatabaseNICS)); }
        }

        private bool m_DatabaseNICSLA = false;
        public bool DatabaseNICSLA
        {
            get { return m_DatabaseNICSLA; }
            set { m_DatabaseNICSLA = value; OnPropertyChanged(nameof(DatabaseNICSLA)); }
        }

        private bool m_DatabaseInterpol = false;
        public bool DatabaseInterpol
        {
            get { return m_DatabaseInterpol; }
            set { m_DatabaseInterpol = value; OnPropertyChanged(nameof(DatabaseInterpol)); }
        }

        private bool m_DatabaseUKPol = false;
        public bool DatabaseUKPol
        {
            get { return m_DatabaseUKPol; }
            set { m_DatabaseUKPol = value; OnPropertyChanged(nameof(DatabaseUKPol)); }
        }

        private bool m_DatabaseBadpol = false;
        public bool DatabaseBadpol
        {
            get { return m_DatabaseBadpol; }
            set { m_DatabaseBadpol = value; OnPropertyChanged(nameof(DatabaseBadpol)); }
        }

        // ------------------------

        private bool m_WeaponM4A1 = false;
        public bool WeaponM4A1
        {
            get { return m_WeaponM4A1; }
            set { m_WeaponM4A1 = value; OnPropertyChanged(nameof(WeaponM4A1)); }
        }

        private bool m_WeaponCV47 = false;
        public bool WeaponCV47
        {
            get { return m_WeaponCV47; }
            set { m_WeaponCV47 = value; OnPropertyChanged(nameof(WeaponCV47)); }
        }

        private bool m_WeaponSword = false;
        public bool WeaponSword
        {
            get { return m_WeaponSword; }
            set { m_WeaponSword = value; OnPropertyChanged(nameof(WeaponSword)); }
        }

        private bool m_WeaponDiamondSword = false;
        public bool WeaponDiamondSword
        {
            get { return m_WeaponDiamondSword; }
            set { m_WeaponDiamondSword = value; OnPropertyChanged(nameof(WeaponDiamondSword)); }
        }

        private bool m_WeaponCandlestick = false;
        public bool WeaponCandlestick
        {
            get { return m_WeaponCandlestick; }
            set { m_WeaponCandlestick = value; OnPropertyChanged(nameof(WeaponCandlestick)); }
        }

        private bool m_WeaponSteamrollerDriven = false;
        public bool WeaponSteamrollerDriven
        {
            get { return m_WeaponSteamrollerDriven; }
            set { m_WeaponSteamrollerDriven = value; OnPropertyChanged(nameof(WeaponSteamrollerDriven)); }
        }

        private bool m_WeaponSteamrollerDropped = false;
        public bool WeaponSteamrollerDropped
        {
            get { return m_WeaponSteamrollerDropped; }
            set { m_WeaponSteamrollerDropped = value; OnPropertyChanged(nameof(WeaponSteamrollerDropped)); }
        }

        private bool m_WeaponSpoon = false;
        public bool WeaponSpoon
        {
            get { return m_WeaponSpoon; }
            set { m_WeaponSpoon = value; OnPropertyChanged(nameof(WeaponSpoon)); }
        }

        // ------------------------

        private string m_ActiveImage = "pack://application:,,,/BaddieTracker;component/Resources/good1.png";
        public string ActiveImage
        {
            get { return m_ActiveImage; }
            set { m_ActiveImage = value; OnPropertyChanged(nameof(ActiveImage)); }
        }

        public void SetActiveImage(string imgname)
        {
            ActiveImage = $"pack://application:,,,/BaddieTracker;component/Resources/{imgname}.png";
        }

        private bool m_IsSearchButtonEnabled = true;
        public bool IsSearchButtonEnabled
        {
            get { return m_IsSearchButtonEnabled; }
            set { m_IsSearchButtonEnabled = value; OnPropertyChanged(nameof(IsSearchButtonEnabled)); }
        }

        private string m_SearchText = "";
        public string SearchText
        {
            get { return m_SearchText; }
            set { m_SearchText = value; OnPropertyChanged(nameof(SearchText)); }
        }

        private double m_ProgressAmount = 0.0;
        public double ProgressAmount
        {
            get { return m_ProgressAmount; }
            set { m_ProgressAmount = value; OnPropertyChanged(nameof(ProgressAmount)); }
        }

        private const string m_defaultProgressLabel = "Ready.";
        private string m_ProgressLabel = m_defaultProgressLabel;
        public string ProgressLabel
        {
            get { return m_ProgressLabel; }
            set { m_ProgressLabel = value; OnPropertyChanged(nameof(ProgressLabel)); }
        }

        public void ResetSearch()
        {
            ProgressLabel = m_defaultProgressLabel;
            ProgressAmount = 0.0;
            IsSearchButtonEnabled = true;
        }

        // ------------------------

        private bool m_AdminTrackingUnrestricted = false;
        public bool AdminTrackingUnrestricted
        {
            get { return m_AdminTrackingUnrestricted; }
            set { m_AdminTrackingUnrestricted = value; OnPropertyChanged(nameof(AdminTrackingUnrestricted)); }
        }

        public bool IsAdminPanelVisible
        {
            get
            {
                return (LocatorMDMA && !ShowdownMinions && ShowdownExplosions
                 && TrackingZoom && TrackingHeroStance && LabTestMontageDuration == 15 && LabTestMontageWorkDone == 7
                 && DatabaseInterpol);
            }
        }

        public Visibility AdminPanelVisibility
        {
            get { return IsAdminPanelVisible ? Visibility.Visible : Visibility.Collapsed; }
        }

        /*
        private bool m_pname = false;
        public bool pname
        {
            get { return m_pname; }
            set { m_pname = value; OnPropertyChanged(nameof(pname)); }
        }
        */



        protected void OnPropertyChanged(string name)
        {
            _OnPropertyChanged(name);
            _OnPropertyChanged("AdminPanelVisibility");
        }

        protected void _OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}

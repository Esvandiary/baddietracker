﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaddieTracker
{
    public static class Config
    {
        public static readonly string SearchName = "Bobby Tables";

        public static readonly Dictionary<string, int> ImageSets = new Dictionary<string, int>
        {
            { "good", 9 },
            { "entrance", 6 },
            { "justdance", 6 },
            { "herostance", 5 },
            { "nozoom", 4 },
            { "wrong", 9 },
            { "jimmy", 5 },
        };

    }
}
